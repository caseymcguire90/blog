class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	#this says that a post has many comments and 
	#it also says that all dependencies (ie comments)
	#will be destroyed along with it.
	validates_presence_of :title
	validates_presence_of :body
end
